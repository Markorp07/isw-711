const path = require('path');
const express = require('express');
const mongoose = require('mongoose');
const app = express();

// Conecting to db
mongoose.connect('mongodb://localhost/crud-students')
    .then(db  => console.log('Db connected'))
    .catch(err => console.log(err));

//JWT Authentication
app.use(function (req, res, next) {
    if (req.headers["authorization"]) {
      const authToken = req.headers['authorization'].split(' ')[1];
      try {
        jwt.verify(authToken, theSecretKey, (err, decodedToken) => {
          if (err || !decodedToken) {
            res.status(401);
            res.json({
              error: "Unauthorized "
            });
          }
          console.log('Welcome', decodedToken.name);
          next();
           if (decodedToken.userId == 123) {
            next();
           }
        });
      } catch (e) {
        next();
      }
  
    } else {
      res.status(401);
      res.send({
        error: "Unauthorized "
      });
    }
  });

// importing routes
const indexRoutes = require('./routes/index');

//setings
app.set('views', path.join(__dirname,'views'));
app.set('view engine', 'ejs');
// middelwares
app.use(express.urlencoded({ extended: false}));
// routes
app.use('/video', indexRoutes);
// Starting server
app.listen(3000, () =>{
    console.log('Server on port 3000');
});

