const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const StudentSchema = new Schema({
    firstname: String,
    lastname: String,
    email: String,
    address: String
});

module.exports = mongoose.model('students',StudentSchema);