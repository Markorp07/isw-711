var express = require("express");
var app = express();

app.get("/hello", (req, res, next) => {
    res.json(`Response: Hello ${req.query.message}`);
   });
app.listen(3000, () => {
console.log("Server running on port 3000");
});