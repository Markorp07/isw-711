const path = require('path');
const express = require('express');
const mongoose = require('mongoose');
const app = express();

// Conecting to db
mongoose.connect('mongodb://localhost/crud-students')
    .then(db  => console.log('Db connected'))
    .catch(err => console.log(err));

// importing routes
const indexRoutes = require('./routes/index');

//setings
app.set('views', path.join(__dirname,'views'));
app.set('view engine', 'ejs');
// middelwares
app.use(express.urlencoded({ extended: false}));
// routes
app.use('/', indexRoutes);
// Starting server
app.listen(3000, () =>{
    console.log('Server on port 3000');
});