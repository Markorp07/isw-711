$(function(){
    $(document).ready(function() {
        $.ajax({
            url: "https://api.openbrewerydb.org/breweries",
            success: function(breweries){
                let tbody = $('tbody');
                tbody.html('');
                for (let i = 0; i < 10; i++) {
                    tbody.append(`
                        <tr>
                            <td class="id">${breweries[i].id}</td>
                            <td>${breweries[i].name}</td>
                            <td><button type="button" class="btn btn-info details-button" id="details-button">Detalles</button></td>
                        </tr>
                    `)
                }
            }
        });
    });
    $('table').on('click','.details-button', function(){
        $('#myModal').modal('show');
        let row = $(this).closest('tr');
        let id = row.find('.id').text();
        $.ajax({
            url: "https://api.openbrewerydb.org/breweries/" + id,
            success: function(breweries){
                console.log(breweries);
                $("#lblName").text("Nombre: " + breweries.name);
                $("#lblType").text("Tipo: " + breweries.brewery_type);
                $("#lblStreet").text("Calle: " + breweries.street);
                $("#lblCity").text("Ciudad: " + breweries.city);
                $("#lblState").text("Estado: " + breweries.state);
                $("#lblPostalCode").text("Código Postal: " + breweries.postal_code);
                $("#lblCountry").text("País: " + breweries.country);
                $("#lblLongitude").text("Longitud: " + breweries.longitude);
                $("#lblLatitude").text("Latitud: " + breweries.latitude);
                $("#lblPhone").text("Teléfono: " + breweries.phone);
                $("#lblWebSite").text("WebSite: " + breweries.website_url);

            }
        });
    });
});